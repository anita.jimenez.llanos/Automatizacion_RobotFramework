# Introduction 
Robot Framework es un marco de automatización genérico de código abierto. Se puede utilizar para la automatización de pruebas y la automatización de RPA (Robotic Process Automation)
Robot Framework se encuentra sobre Python.

# Requerimientos
python 3.10.5
RobotFramework
pip install robotframework
pip install robotframework-selenium2library
pip install webdrivermanager
pip install robotframework-httplibrary robotframework-requests
pip install gherkin2robotframework

Instalar paquetes para excel,csv y Bdd
pip install robotframework-datadriver
pip install robotframework-exceldatadriver
pip install -U robotframework-datadriver[XLS]
pip install robotframework-databaselibrary
pip install robotframework-pabot

Instalar paquetes API Testing
python -m pip install -U setuptools
pip install requests
pip install    robotframework-requests
pip install robotframework-jsonlibrary
pip install jsonpath-rw
pip install jsonpath-rw-ext

# Build and Test
robot -d Resultados testCase/SimulacionCompraTelefonos.robot
robot -d result testCase/ApiTestSingUp.robot
robot -d result testCase/ApiTestLogin.robot