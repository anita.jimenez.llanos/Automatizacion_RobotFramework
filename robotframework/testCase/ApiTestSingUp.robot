*** Settings ***
Library     DataDriver      ../resources/files/testData/ApiTestSingUp.csv
Resource    ../testCase/ApiTestSingUp_step_definitions.robot
Test Template       Scenario Outline: T-API-PQBP-603-CA1- Api cliente signup

*** Test Cases ***
Scenario: Consultar clinte y obtener su informacion basica ${username}     ${password}
    [Tags]      @REQ_PQBP-602

*** Keywords ***
Scenario Outline: T-API-PQBP-603-CA1- Api cliente signup
    [Tags]      @id:1 @ApiTestSingUp @consultaCliente
    [Arguments]     ${username}     ${password}
    Given consulto cliente username, password y valido    ${username}     ${password}
