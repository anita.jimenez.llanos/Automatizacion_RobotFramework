*** Settings ***
Library     DataDriver      ../resources/files/testData/ApiTestLogin.csv
Resource    ../testCase/ApiTestLogin_step_definitions.robot
Test Template       Scenario Outline: T-API-PQBP-602-CA1- Api cliente login

*** Test Cases ***
Scenario: Consulta api login cliente ${username}     ${password}
    [Tags]      @REQ_PQBP-602

*** Keywords ***
Scenario Outline: T-API-PQBP-602-CA1- Api cliente login
    [Tags]      @id:1 @ApiTestLogin @consultaCliente
    [Arguments]     ${username}     ${password}
    Given consulto cliente username, password y valido    ${username}     ${password}
