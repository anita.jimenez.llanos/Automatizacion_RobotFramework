*** Settings ***
Library     SeleniumLibrary
Resource    ../resources/files/keywords/Common.robot
Resource    ../resources/files/keywords/SimulacionCompraTelefonos_keywords.robot
Library     OperatingSystem
Library     String


*** Keywords ***
el cliente ingresa a la pagina de demoblaze para la compra de telefonos selecciona el producto y verifica precio
    Opening Browser     ${url}  ${navegador}
    Seleccionar producto y agregar a la compra
    Carro de compras

el decide hacer la compra ingresa sus datos personales name, country, city, card, month y year
    [Arguments]     ${name}  ${country}  ${city}  ${card}  ${month}  ${year}
    DatosCliente   ${name}  ${country}  ${city}  ${card}  ${month}  ${year}

el realiza la compra del producto exitosamente
    [Documentation]     Compra Exitosa
    [Tags]      @compraExitosa
    Confirmacion
    Cerrar la pagina