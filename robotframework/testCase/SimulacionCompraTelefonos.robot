*** Settings ***
Library     DataDriver      ../resources/files/testData/Datos.xlsx         sheet_name=Hoja1
Resource    ../testCase/SimulacionCompraTelefonos_step_definitions.robot
Test Template       Scenario Outline: T-E2E-PQBP-609-CA1- Simulacion de la Compra de un telefono iphone 6 y Samsung galaxy s7

*** Test Cases ***
Scenario: Simulacion de la Compra de un telefono iphone 6 y samsung S10  ${name}  ${country}  ${city}  ${card}  ${month}  ${year}
    [Tags]      @REQ_PQBP-602


*** Keywords ***
Scenario Outline: T-E2E-PQBP-609-CA1- Simulacion de la Compra de un telefono iphone 6 y Samsung galaxy s7
    [Tags]   @id=1
    [Arguments]     ${name}  ${country}  ${city}  ${card}  ${month}  ${year}
    Given el cliente ingresa a la pagina de demoblaze para la compra de telefonos selecciona el producto y verifica precio
    When el decide hacer la compra ingresa sus datos personales name, country, city, card, month y year  ${name}  ${country}  ${city}  ${card}  ${month}  ${year}
    Then el realiza la compra del producto exitosamente
