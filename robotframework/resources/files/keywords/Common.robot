*** Settings ***
Library  SeleniumLibrary

**Variables***
${url}=     https://www.demoblaze.com/
${navegador}=    chrome   

*** Keywords ***
Opening Browser
    [Arguments]     ${url}   ${navegador}
    Open Browser    ${url}      ${navegador}
    maximize browser window 	
    Title Should Be     STORE
    set selenium implicit wait    .5
    #set selenium speed    .5s    