*** Settings ***
Library     SeleniumLibrary

*** Variables ***
${ruta_Imagen}=     ../result/screenshot/

*** Keywords ***
F_texto     
    [Documentation]     Funcion para selector texto "input text"
    [Arguments]     ${sel}      ${dato}
    Wait Until Element Is Enabled       ${sel}
    Wait Until Element Is Visible       ${sel}
    Input Text      ${sel}      ${dato}
    Capture Element Screenshot      ${sel}      ${ruta_Imagen}${dato}.png         