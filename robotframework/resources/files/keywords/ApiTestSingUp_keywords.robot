*** Settings ***
Documentation           Consulta api SingUp

Library     SeleniumLibrary
Library     RequestsLibrary
Library     Collections

*** Variables ***
${url_api}=      https://api.demoblaze.com/

*** Keywords ***
Consulta api SingUp
    [Documentation]    TC que consulta api singUp
    [Arguments]     ${username}     ${password}
    Create Session      urlAPi      ${url_api}
    ${body}=        Create Dictionary       username=${username}        password=${password}
    ${header}=      Create Dictionary       Content-Type=application/json

    ${response}=        Post Request      urlAPi      signup        data=${body}        headers=${header}

    Log To Console      ${response.status_code}
    Log To Console      ${response.content}
    Log To Console      ${response.headers}

    #Validaciones
    ${status}=      Convert To String       ${response.status_code}
    Should be equal     ${status}       200
