*** Settings ***
Library     SeleniumLibrary
Resource    ../keywords/funciones.robot
Variables       ../locators/compraTelefonos.py
#Variables       ../testData/dataCliente.py

***Variables****
${ruta_Imagen}=     ../result/screenshot/

*** Keywords ***
Seleccionar producto y agregar a la compra
    Click Link  ${producto1}
    Click Link  ${btn_cart}
    Handle Alert   accept
    Click Link  ${home}
    Click Link  ${producto2}
    Click Link  ${btn_cart}
    alert should be present     Product added

Carro de compras
    Click Link      ${cart}
    click Button    ${btn_PlaceOrder}

DatosCliente
    [Arguments]  ${name}  ${country}  ${city}  ${card}  ${month}  ${year}
    F_texto    ${textName}      ${name}
    F_texto    ${textCountry}   ${country}
    F_texto    ${textCity}      ${city}
    F_texto    ${textCard}      ${card}
    F_texto    ${textMonth}     ${month}
    execute javascript      window.scrollTo(0,400) 
    F_texto    ${textYear}      ${year}
    click button    ${btn_Purchase}

Confirmacion
    wait until page contains    Thank you for your purchase!
    click button   ${btn_ok}
    Capture Page Screenshot     ${ruta_Imagen}confirmacion.png

Cerrar la pagina 
    sleep    1
    close browser